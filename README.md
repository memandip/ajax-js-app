# ajax-js-json-app

#Requirements
You will need an active server for this application to work properly.

This is a simple application which reads the json file and loops through
the content to display.

#This app contains three small apps

# 1) live search
This app simply contains the search field which will search through the data.json file to search the results. The search function is called on keyup event.

# 2) Content slider
It also reads the data.json the file and displays the content of the data.json file. The content will automatically slide.

# 3) Image slider
It also reads the data.json the file and displays the content of the data.json file. This is the simple image slider.
